function drawNameWithLines ()
{
  // insert your code here to draw the letters of your name 
  // using only lines()
  line (100, 100, 200, 100);
  line (150, 200, 150, 100);
  line (100, 200, 150, 200);
  line (250, 100, 250, 200);
  line (250, 150, 350, 150);
  line (350, 100, 350, 200);
}

function drawNameWithTriangles ()
{
  // insert your code here to draw the letters of your name 
  // using only ltriangles()
    triangle (75, 250, 75, 275, 250, 275);
    triangle (75, 250, 250, 275, 250, 250);
    triangle (150, 275, 150, 425, 175, 425);
    triangle (175, 425, 175, 275, 150, 275);
    triangle (75, 425, 75, 400, 150, 425);
    triangle (150, 400, 75, 400, 150, 425);
    triangle (300, 250, 300, 425, 325, 425);
    triangle (300, 250, 325, 250, 325, 425);
    triangle (325, 310, 325, 335, 375, 335);
    triangle (325, 310, 375, 310, 375, 335);
    triangle (375, 425, 400, 425, 375, 250);
    triangle (400, 425, 400, 250, 375, 250);
}

// -----------------------------------------------------------
//
//  Do not edit below this lne
//
// -----------------------------------------------------------

let doLine;
let doTri;
let lineColor;
let fillColor;
let backgroundColor;

function setup() {
  createCanvas(500, 500);
  backgroundColor = color (150, 150, 150);
  background(backgroundColor);
  doLine = false;
  doTri = false;
  lineColor = color (0, 0, 0);
  fillColor = color (255, 0, 0);
}

function draw ()
{
  if (doLine) stroke(lineColor); else stroke (backgroundColor);
  drawNameWithLines();
  
  if (doTri) {
     fill(fillColor);
     stroke(fillColor);
  }
  else {
    fill(backgroundColor);
    stroke(backgroundColor);
  }
  drawNameWithTriangles();
}

function keyPressed()
{
  if (key == 'l') doLine = !doLine;
  if (key == 't') doTri = !doTri;
}